package kz.aitu.project.repository;

import kz.aitu.project.entity.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findAll();



    @Query(value = "INSERT INTO customer(customer_id, customer_password) VALUES (:customer_id, :customer_password)", nativeQuery = true)
    void createCustomer(@Param("customer_id") long customer_id, @Param("customer_password") String customer_password);
}

