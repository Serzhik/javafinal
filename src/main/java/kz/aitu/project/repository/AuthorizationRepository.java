package kz.aitu.project.repository;

import kz.aitu.project.entity.Authorization;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {
    
    Authorization findByLoginAndPassword(String login, String password);

    Authorization findByToken(String token);
    Authorization createAccount(String login, String password) ;

    @Query(value = "INSERT INTO auth(id, password) VALUES (:customer_id, :customer_password)", nativeQuery = true)
    void createAccount(@Param("customer_id") long customer_id, @Param("customer_password") String customer_password);
}


