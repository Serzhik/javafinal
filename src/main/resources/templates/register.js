var app = angular.module('aitu-project', []);
app.controller('ProductCtrl', function($scope, $http) {



    $scope.authorization = {
        login: '',
        password: '',
        token: ''
    };

    $scope.customer = {};

    $scope.login = function(authorization) {
        $http({
            url: 'http://127.0.0.1:9090/api/login',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },
            data: authorization
        })
            .then(function (response) {
                    $scope.authorization = response.data;
                    $scope.getMe();

                },
                function (response) { // optional
                    $scope.authorization = {}
                });
    };



    $scope.getMe = function(){
        $http({
            url: 'http://127.0.0.1:9090/api/customers/me',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Authorization":$scope.authorization.token
            }
        })
            .then(function (response) {
                    $scope.customer = response.data;
                },
                function (response) { // optional
                    console.log(response);
                    $scope.customer ={};
                    $scope.shopMessage = 'Login or Password is incorrect!';
                });
    };


});


